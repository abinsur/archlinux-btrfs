<div align="center">

[<img src="https://i.imgur.com/UjnRFbR.png" width="500">](https://archlinux.org/)

[<img src="https://btrfs.wiki.kernel.org/images-btrfs/archive/f/f5/20210218163832%21Btrfs_logo_small.png">](https://en.wikipedia.org/wiki/Btrfs)

</div>

### create file system
>`# mkfs.fat -F32 -I /dev/sdX1`   
>256M
>
>`# mkswap /dev/sdX2 && swapon /dev/sdX2`   
>3G
>
>`# mkfs.btrfs -f /dev/sdX3`   
>*G

### mount root partition
`# mount /dev/sdX3 /mnt`   
`# cd /mnt`

### create btrfs subvolumes
`# btrfs su cr @`   
>root
   
>>su-subvolume   
>>cr-create

`# btrfs su cr @home`   
>home

`# btrfs su cr @snapshots`   
>snapshots  

`# btrfs su cr @var_log`   
>var_log  


### mount subvolumes
`# umount -Rv /mnt`

`# mount -o defaults,noatime,compress=zstd,space_cache=v2,subvol=@ /dev/sdX3 /mnt`   

`# mkdir -p /mnt/{boot,home,.snapshots,var/log}`   
 
`# mount /dev/sdaX /mnt/boot`   
`# mount -o defaults,noatime,compress=zstd,space_cache=v2,subvol=@home /dev/sdX3 /mnt/home`   
`# mount -o defaults,noatime,compress=zstd,space_cache=v2,subvol=@snapshots /dev/sdX3 /mnt/.snapshots`   
`# mount -o defaults,noatime,compress=zstd,space_cache=v2,subvol=@var_log /dev/sdX3 /mnt/var/log`   

### install archlinux base
`# pacstrap /mnt base base-devel linux linux-firmware git vim`   
`# genfstab -U /mnt > /mnt/etc/fstab`  

### chrooted
`# arch-chroot /mnt`  
`# pacman -S --needed snapper snap-pac btrfs-progs grub-btrfs rsync mtools dosfstools`    

### initram
>`# vim /etc/mkinitcpio.conf`  
>add in MODULES=(btrfs)   

`# mkinitcpio -p linux`   

### bootloader
`# grub-install /dev/sdX`   
`# grub-mkconfig -o /boot/grub/grub.cfg`   

### ... make a [basic configuration](https://wiki.archlinux.org/title/Installation_guide#Time_zone)

`# passwd root`  

`# useradd -m -G wheel -s /bin/bash archer`   
`# passwd archer`   



### snapper configuration

`# umount /.snapshots`   
`# rm -rf /.snapshots`   
`# snapper -c root create-config /`   
`# btrfs subvolume list /`   
`# btrfs subvolume delete /.snapshots`   
`# mkdir /.snapshots`   

`# mount -a`  
`# chmod 750 /.snapshots`   

>`# vim /etc/snapper/config/...`   
> add in ALLLOW_USERS=archer      
> ...explorer more options...   

`# systemctl enable snapper-timeline.timer`   
`# systemctl enable snapper-cleanup.timer`   
`# systemctl enable grub-btrfs.path`   

### more snapper

`$ git clone https://aur.archlinux.org/yay`    
`$ cd yay`   
`$ makepkg -si PKGBUILD`   
`$ yay --combinedupgrade --editmenu --nodiffmenu --save`    
`$ yay -S snapper-gui-git`   

### hooks
~~~
mkdir -p /etc/pacman.d/hooks
> /etc/pacman.d/hoooks/30-bootbackup.hook
cat > /etc/pacman.d/hoooks/30-bootbackup.hook << "EOF"  
[Trigger]   
Operation=Upgrade   
Operation=install   
Operation=Remove   
Type=Path   
Target=boot/*   
   
[Action]   
Depends=rsync   
Description=Copying the /boot partition....   
When=PreTransaction   
Exec=/user/bin/rsync -a --delete /boot /.bootbackup   
EOF
~~~

### things
`# chmod a+rx /.snapshots`   
`# chown :archer /.snapshots`

-------

### exemplos para deletar subvolumes

`# btrfs subvolume delete nomeoucaminhodosubvolume`


### exemplo de uma instalação real, onde o subvolume no qual o sistema está instalado se chama "@"

`# btrfs subvolume show /`  
`# btrfs subvolume show /home`  
`# btrfs filesystem df /`  
`# btrfs filesystem usage /`  
`# mount -o subvol=sistema compress=zstd:6 /dev/sda2 /mnt`  


### execute estes comandos quando a máquina tiver algum tempo ocioso

`# btrfs filesystem defrag -v -r -f /`   
`# btrfs filesystem defrag -v -r -f / home`   
`# btrfs balance start -m /`   
`$ sudo btrfs fi defrag -r /path`   

<div align="center">

Copyright © 2002-2022 Judd Vinet, Aaron Griffin and Levente Polyák.    
The [Arch Linux](https://archlinux.org/) name and logo are recognized trademarks. Some rights reserved.   
[Linux®](https://www.kernel.org/) is a registered trademark of Linus Torvalds.   

</div>
